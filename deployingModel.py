# import the opencv library
import cv2
import tensorflow as tf
import numpy as np
from pygame import mixer


vid = cv2.VideoCapture(0)
model_face = tf.keras.models.load_model("result/bestValLoss.hdf5")
face_cascade = cv2.CascadeClassifier(cv2.data.haarcascades +'haarcascade_frontalface_default.xml')

font = cv2.FONT_HERSHEY_SIMPLEX

mixer.init()
sound = mixer.Sound('alarm.wav')

while(True):
  ret, frame = vid.read()

  gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
  faces = face_cascade.detectMultiScale(gray, 1.1, 4)
  for (x,y, w, h) in faces :
    face = gray[y:y + h, x:x + w]
    face = cv2.resize(face, (128,128))
    face = face.reshape(1,128,128,1)
    result = model_face.predict(face)

    if(result[0]>0.5):
      # text = {"{:.2f}".format(result[0]*100.) }
      cv2.rectangle(frame, (x, y), (x+w, y+h), (0, 255, 0), 2)
      text = result[0]*100.
      cv2.putText(frame, 
              f'MASK : {text}', 
              (x, y), 
              cv2.FONT_HERSHEY_SIMPLEX, 
              1/2, 
              (0, 255, 0 ), 
              1, 
              cv2.LINE_AA)

    elif(result[0]<=0.5):
      cv2.rectangle(frame, (x, y), (x+w, y+h), (0, 0, 255), 2)
      # text = "{:.2f}".format(100. - result[0]*100.)
      text = 100. - result[0]*100.
      cv2.putText(frame, 
              f'NO MASK : { text }', 
              (x, y), 
              cv2.FONT_HERSHEY_SIMPLEX, 
              1/2, 
              (0, 0, 255 ), 
              1, 
              cv2.LINE_AA)
      if(mixer.get_busy() is False):
        sound.play()

  cv2.imshow('MASK DETECTION', frame)

  if cv2.waitKey(1) & 0xFF == ord('q'):
    break

vid.release()
cv2.destroyAllWindows()
